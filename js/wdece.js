var map;
var markerArray = [];

function initMap() {
  setMapSize();
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 43.286349, lng: -107.632578},
    zoom: 6,
    mapTypeId: google.maps.MapTypeId.ROADMAP, // ADDED This line down to 21 to try and add county layer
    styles: [
      {
        featureType: 'poi',
        stylers: [{visibility: 'off'}]
     },
     {
       featureType: 'poi.park',
       stylers: [{visibility:'on'},{color:'#cccccc'}]
     },
    {
      featureType: 'landscape',
      stylers: [{color:'#eeeeee'}]
    },
    {
      featureType: 'landscape.man_made',
      stylers: [{color:'#dddddd'}]
    },
    {
      featureType: 'landscape.natural.terrain',
      stylers: [{color:'#333333'}]
    },
    {
      featureType: 'road',
      elementType: 'geometry.fill',
      stylers: [{color:'#000000'}]
    },
    {
      featureType: 'road',
      elementType: 'geometry.stroke',
      stylers: [{color:'#ffffff'}]
    },
    {
      featureType: 'road',
      elementType: 'labels.text',
      stylers: [{color:'#ffffff'}]
    },
    {
      featureType: 'road',
      elementType: 'labels.text.fill',
      stylers: [{color:'#000000'}]
    }
    ]

  });

  map.setOptions({ draggable : false });

  var layer = new google.maps.FusionTablesLayer({
    query: {
      select: 'geometry',
      from: '12Gy3F1bD3ilLd0UmXccwm4vgGDw9TlCliApFntKr'
      // Link to our own copy of the table: https://fusiontables.google.com/data?docid=12Gy3F1bD3ilLd0UmXccwm4vgGDw9TlCliApFntKr
    },
    styles: [{
      polygonOptions: {
        fillColor: '#FFFFFF',
        fillOpacity: 0.01
      }
    }],
    map: map,
    styleId: 4,
    templateId: 12
    });
}

function setMapSize() {
  var margin = $('.navbar-fixed-top').height();
  var h = $(window).height()-$('.navbar-fixed-top').height()-$('.navbar-fixed-bottom').height();
  var w = $(window).width();
  $('#map').css({'height':h,'width':'100%','margin-top':margin});
}

$(document).ready(function() {
  let slct = $('select');
  slct.hide();

  var inpu = $('input');

  function _resetSelect() {
    slct.html('');
    slct.hide();
  }

  function _select(dataItem) {
    inpu.val(dataItem['socTitle']);
    _resetSelect();
    inpu.focus();
    showDetail(dataItem);
  }

  function _showingSelect() {
    return (slct.css('display') != 'none')
  }

  inpu.on('keyup',function(event){
    if (event.keyCode == 40 && _showingSelect()) {
      slct.focus();
    } else {
      var q = $(event.target).val();
      if (q.length) {
        $.ajax({
          url: "http://wdece-services.azurewebsites.net/api/Occupations/SearchTitle?title="+encodeURIComponent(q)+"&maxResults=10"
        }).done(function(data) {
          if (data.status == 1) {
            console.log(data);
            slct.html('');
            var count = data.items.length;
            for (var n = 0; n < count; n++) {
              var opt = $('<option>');
              opt.attr('data-id', data.items[n]['occupationId']);
              opt.attr('value', n);
              opt.html(data.items[n]['socTitle']);
              opt.click(function(e) {
                var x = $(e.target).val();
                _select(data.items[x]);
              });
              slct.append(opt);
            }
            slct.off('keyup');
            slct.on('keyup', function(e) {
              if (e.keyCode == 13) {
                e.preventDefault();
                var n = $(e.target).val()||$(e.target).closest('select').val();
                _select(data.items[n]);
              }
            });
            slct.show();
          } else {
            console.log('Response to query reports an error.');
            console.log(data);
          }
          });
      } else {
        _resetSelect();
      }
    }
  });

  var detail = $("#career-result");
  var detailPanel = $('#detail-panel');

  inpu.click(function(e) {
    detailPanel.collapse('hide');
  });

  var tables = ['Tasks','RelatedCareers','Knowledges','Skills'];

  function showDetail(dataItem) {
    detail.show();
    detailPanel.collapse('show');
    detail.find('h3 a').html(dataItem['socTitle']);
    $('#collapseOne div').html(dataItem['socDefinition']);
    getLatLong(dataItem['occupationId']);
    $(tables).each(function(i,table) {
      showFromSocCode(table,dataItem['socCode'],show)
    });
  }

  function show(table,data) {
    console.log(data);
    //TODO: Show data in UI
    var location = '';
    switch (table) {
      case "Tasks":
        location = '#collapseTwo ul';
        break;
      case 'RelatedCareers':
        location = '#collapseFive ul'; //Need to figure out code to show data from here.
        break;
      case 'Knowledges':
        location = '#collapseThree ul';
        break;
      case 'Skills':
        location = '#collapseFour ul'
        break;
      default:
        console.log("Error");
//supposed to show different data in UI, based on section
    }
    for (var i = 0; i < data.length; i++){
      var li = $('<li></li>');
      li.html(data[i]['description']);
      $(location).append(li);
    }
  }

  function showFromSocCode(table,socCode,callback) {
    $.ajax({
      url: "http://wdece-services.azurewebsites.net/onet-api/"+table+"/"+encodeURIComponent(socCode)
    }).done(function(data) {
      console.log('Showing '+table);
      callback(table,data);
    });
  }

  // Adding Markers

  function displayMarker(lat,lon,label) {
    var marker = new google.maps.Marker({
       position: new google.maps.LatLng(lat,lon),
       map: map
    });

    markerArray.push(marker);

    var infowindow = new google.maps.InfoWindow({
      content: label
    });

    marker.addListener('mouseover', function() {
      infowindow.open(map, marker);
    });
    marker.addListener('mouseout', function() {
      infowindow.close(map, marker);
    });
  }

  function getLatLong(occupationId) {
    $.ajax({
      url: "http://wdece-services.azurewebsites.net/api/MapMarker/CompanyOccupation/"+encodeURIComponent(occupationId)
    }).done(function(data) {
      mapOccupations(data);
    });
  }

  function mapOccupations(data) {
    for (i=0; i<markerArray.length; i++){
      markerArray[i].setMap(null);
    }

    markerArray = [];

    for(i=0; i<data.length; i++){
      displayMarker(data[i]['position']['lat'], data[i]['position']['lng'], data[i]['label']);
    }
  }

  $(".colorToggle").click(function(e){
    $("body").removeClass();
    var palette = e.target.id;
    $("body").addClass(palette);
  });
  $(".default").click(function(e){
    $("body").removeClass();
  });
});
